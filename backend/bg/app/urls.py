from django.urls import path 

from .views import keygeneratorListView,keygeneratorDetailView

urlpatterns=[
    path('',keygeneratorListView.as_view()),
    path('<pk>',keygeneratorListView.as_view()),
]