from django.shortcuts import render

from rest_framework.generics import  ListAPIView,RetrieveAPIView

from rest_framework import serializers

from .serilizers import keygeneratorSerializers

from .models import keygenerator

class keygeneratorListView(ListAPIView):
    queryset = keygenerator.objects.all()
    serializer_class = keygeneratorSerializers


class keygeneratorDetailView(RetrieveAPIView):
    queryset = keygenerator.objects.all()
    serializer_class = keygeneratorSerializers
