from .models import keygenerator
from rest_framework import serializers

class keygeneratorSerializers(serializers.Serializer):
    class Meta:
        model=keygenerator
        fields=('title','content')